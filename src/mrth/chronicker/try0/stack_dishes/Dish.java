package mrth.chronicker.try0.stack_dishes;

public abstract class Dish implements Fragile{
   private boolean brokenState = false;
   protected final String color;

   protected Dish() {
      this("White");
   }

   protected Dish(String color) {
      this.color = color;
   }

   public void setBrokenState(boolean state) {
      if (!brokenState) {
         brokenState = state;
      }
   }

   public boolean getBrokenState() {
      return brokenState;
   }

   public String getState() {
      if (brokenState) {
         return "broken";
      }
      return "unbroken";
   }
}

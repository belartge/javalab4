package mrth.chronicker.try0.stack_dishes;

import java.util.Random;
import java.util.Stack;

public class BunchOfDishes {
   private Stack<Dish> dishStack = new Stack<>();
   Random random = new Random();

   public int getDishCount() {
      return dishStack.size();
   }

   public void putADish(Dish dish) {
      dishStack.push(dish);

      if (getDishCount() > 10) {
         System.out.println("Attention: unstable stack!");
      }

      if (shouldBeFallen()) {
         System.out.println("Oh no! It's falling!!!");
         int count = howMushShouldBeBroken();
         for (int i = 0; i < count; i++) {
            Dish poorDish = dishStack.pop();
            poorDish.beBroken();
         }
      }
   }

   private boolean shouldBeFallen() {
      return (getDishCount() > 8) && (random.nextInt(500 - 5 * getDishCount()) % 2 == 0);
   }

   private int howMushShouldBeBroken() {
      return random.nextInt(getDishCount());
   }
}

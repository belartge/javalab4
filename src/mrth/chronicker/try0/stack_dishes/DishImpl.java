package mrth.chronicker.try0.stack_dishes;

public class DishImpl extends Dish {

   @Override
   public void beBroken() {
      this.setBrokenState(true);
      System.out.println("tink");
   }

   @Override
   public String toString() {
      return getState() + " " + color + " dish";
   }
}

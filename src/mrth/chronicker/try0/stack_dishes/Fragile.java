package mrth.chronicker.try0.stack_dishes;

public interface Fragile {
   void beBroken();
}

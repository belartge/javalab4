package mrth.chronicker.try0.set_sweets;

import java.util.HashSet;

public class CandyPocket {
   private HashSet<Candy> candies = new HashSet<>();

   public void putCandy(Candy candy) {
      candies.add(candy);
   }

   public Candy getCandy() {
      Candy candy = candies.iterator().next();
      candies.remove(candy);
      return candy;
   }

   public int howManyDoWeHave() {
      int i = 0;
      for (Candy c:
           candies) {
         i++;
      }
      return i;
   }

   public String whatDoWeHave() {
      StringBuilder stringBuilder = new StringBuilder();
      for (Candy candy : candies) {
         stringBuilder.append(candy.toString());
         stringBuilder.append(", ");
      }
      return stringBuilder.toString().substring(0, stringBuilder.toString().length() - 2);
   }
}

package mrth.chronicker.try0.set_sweets;

public class Candy implements Sweet {
   public String color;
   public State taste;

   public Candy() {
      this(State.CARAMEL, "mint");
   }

   public Candy(State taste, String color) {
      this.color = color;
      this.taste = taste;
   }
   
   public enum State {
      CHOCOLATE, CARAMEL
   }

   @Override
   public String toString() {
      return color + " " + getState();
   }

   public String getState() {
      if (taste == State.CARAMEL) {
         return "caramel";
      }
      return "chocolate";
   }
}

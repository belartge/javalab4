package mrth.chronicker.try0.list_dict_fridge;

public class Food {
   private final String name;

   public Food(String name) {
      this.name = name;
   }

   public String getName() {
      return name;
   }

   @Override
   public String toString() {
      return getName();
   }
}

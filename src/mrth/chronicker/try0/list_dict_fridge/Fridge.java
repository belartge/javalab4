package mrth.chronicker.try0.list_dict_fridge;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Random;

public class Fridge {
   private HashMap<Integer, List<Food>> shelfs;
   private final int countOfShelfs;
   private Random random = new Random();

   public Fridge(int countOfShelfs) {
      this.countOfShelfs = countOfShelfs;
      this.shelfs = new HashMap<>();
      for (int i = 0; i < countOfShelfs; i++) {
         this.shelfs.put(i, new ArrayList<>());
      }
   }

   public void putFood(Food food) {
      putFood(food, random.nextInt(countOfShelfs));
   }

   public void putFood(Food food, int at) {
      List<Food> shelf = shelfs.get(at);
      shelf.add(food);
      shelfs.put(at, shelf);
   }

   public Food getFood(int from, String name) {
      List<Food> shelf = shelfs.get(from);
      for (Food food : shelf) {
         if (food.getName().equals(name)) {
            shelf.remove(food);
            return food;
         }
      }
      return null;
   }

   public Food getFood(String name) {
      for (int i =0; i < countOfShelfs; i++) {
         Food food = getFood(i, name);
         if (food != null) {
            return food;
         }
      }
      System.out.println("There is no " + name);
      return null;
   }

   public Food getAnyFood() {
      for (int i = 0; i < countOfShelfs; i++) {
         if (shelfs.get(0).size() > 0) {
            return getFood(i, shelfs.get(0).get(0).getName());
         }
      }
      System.out.println("Your fridge is empty");
      return null;
   }

   @Override
   public String toString() {
      StringBuilder builder = new StringBuilder();
      for (int i = 0; i < countOfShelfs; i++) {
         builder.append(", ");
         builder.append(shelfs.get(i));
      }
      return builder.toString().substring(2);
   }
}

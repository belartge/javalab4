package mrth.chronicker.try0;

import mrth.chronicker.try0.list_dict_fridge.Food;
import mrth.chronicker.try0.list_dict_fridge.Fridge;
import mrth.chronicker.try0.set_sweets.Candy;
import mrth.chronicker.try0.set_sweets.CandyPocket;
import mrth.chronicker.try0.stack_dishes.BunchOfDishes;
import mrth.chronicker.try0.stack_dishes.DishImpl;

public class Main {
   public static void main(String[] args) {
      testListMapFridge();
   }

   public static void testStackOfDishes() {
      BunchOfDishes bunchOfDishes = new BunchOfDishes();
      int dishCount = 20;
      System.out.println("We have " + dishCount + " stack_dishes to wash. \nLet's start washing and counting how much we've already washed");
      for (int i = 0; i < 20; i++) {
         bunchOfDishes.putADish(new DishImpl());
         System.out.println("We've washed " + (i + 1) + " stack_dishes and have a bunch of " + bunchOfDishes.getDishCount() + " stack_dishes");
      }
      System.out.println("Only " + bunchOfDishes.getDishCount() + " stack_dishes remain");
   }

   public static void testSetOfCandies() {
      CandyPocket candyPocket = new CandyPocket();
      candyPocket.putCandy(new Candy());
      candyPocket.putCandy(new Candy(Candy.State.CHOCOLATE, "lemon"));
      candyPocket.putCandy(new Candy(Candy.State.CARAMEL, "lemon"));
      candyPocket.putCandy(new Candy(Candy.State.CHOCOLATE, "chocolate"));
      System.out.println("We have " + candyPocket.whatDoWeHave());

      int candiesCount = candyPocket.howManyDoWeHave();

      for (int i = 0; i < candiesCount; i++) {
         System.out.println("I took " + candyPocket.getCandy());
      }
   }

   public static void testListMapFridge() {
      Fridge fridge = new Fridge(3);
      Food f = fridge.getAnyFood();
      f = new Food("Meat");
      Food f2 = new Food("Potato soup");
      Food f3 = new Food("Rise soup");
      Food f4 = new Food("Candies Pocket");
      Food f5 = new Food("Salad");
      Food f6 = new Food("Eggs");
      Food f7 = new Food("Pineapple");

      fridge.putFood(f, 0);
      fridge.putFood(f2, 1);
      fridge.putFood(f3, 2);
      fridge.putFood(f4);
      fridge.putFood(f5);
      fridge.putFood(f6);
      fridge.putFood(f7);

      System.out.print("The fridge contains: ");
      System.out.println(fridge.toString());
      System.out.println("Random food picked: ");
      System.out.println("\t" + fridge.getAnyFood());
      System.out.println("I want to get rise soup:");
      System.out.println("\t" + fridge.getFood("Rise soup"));
      System.out.println("I want to get an apple");
      System.out.println("\t" + fridge.getFood("Apple"));
   }
}
